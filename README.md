# Project Documentation: Symfony 6 & API Platform 3 with Docker

## Project Overview

This project is a RESTful API service for managing `Car` entities and their associated `Reviews`. It is built using Symfony 6 and API Platform 3, using PHP 8, and backed by a PostgreSQL database. The application is containerized using Docker for easy development and deployment.

## Installation Guide

### Prerequisites

- Docker and Docker Compose
- PHP 8
- Composer
- PostgreSQL Client (optional for local database interaction)

### Setup

1. Clone the repository and navigate to the project directory.
2. Copy `.env.example` to `.env` and adjust the database settings if necessary.
3. Run `composer install` to install PHP dependencies.
4. Build and start Docker containers using `docker-compose up --build`.

### Database Configuration

- The `.env` file contains the database connection configurations.
- Run `php bin/console doctrine:database:create` to create the database.
- Run `php bin/console doctrine:migrations:migrate` to apply migrations.

## API Usage

The API provides standard CRUD operations for `Car` and `Reviews` entities, as well as a custom endpoint to fetch the latest five reviews with a rating above 6 for a given car.

## Testing

Tests are located in the `tests/` directory and can be run using PHPUnit with the command `php bin/phpunit`.

## Dockerization

The `Dockerfile` sets up the PHP environment, and the `docker-compose.yml` defines services for the application and PostgreSQL database.

## Code Examples

### Entities

The `Car` and `Reviews` entities are created with Doctrine annotations and include validation constraints.

### Custom Endpoint

The `ReviewsController` contains a method `getLatestReviews` that utilizes a custom query to fetch the desired reviews.

## Maintenance

Documentation should be updated alongside any changes to the codebase to ensure accuracy.

## Style Guide

Documentation is written in Markdown format for readability and consistency.

## Visual Aids

Diagrams and screenshots can be included where beneficial.

## User Support

FAQs, support channels, and contact information are provided for user assistance.

## Conclusion

The project is designed to be a simple yet powerful example of a RESTful API using Symfony and API Platform with Docker support.

# Docker Configuration Files

### Dockerfile

```Dockerfile
# Dockerfile
FROM php:8.0-fpm

# Install dependencies
RUN apt-get update && apt-get install -y \
    libpq-dev \
    && docker-php-ext-install pdo_pgsql

# Set working directory
WORKDIR /var/www

# Copy application source
COPY . /var/www

# Expose port 9000 and start php-fpm server
EXPOSE 9000
CMD ["php-fpm"]
```

### docker-compose.yml

```yaml
version: '3.8'
services:
  app:
    build: .
    ports:
      - "80:80"
    depends_on:
      - db
  db:
    image: postgres:latest
    environment:
      POSTGRES_DB: symfony
      POSTGRES_USER: user
      POSTGRES_PASSWORD: password
    volumes:
      - dbdata:/var/lib/postgresql/data
volumes:
  dbdata:
```

# Entity Code Examples

### Car.php

```php
// src/Entity/Car.php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Metadata\ApiResource;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
#[ApiResource]
class Car
{
    // ... properties with getters and setters
}
```

### Reviews.php

```php
// src/Entity/Reviews.php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Metadata\ApiResource;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
#[ApiResource]
class Reviews
{
    // ... properties with getters and setters
}
```

# Custom Controller Example

### LatestCarReviewsController.php

```php
// src/Controller/LatestCarReviewsController.php
namespace App\Controller;

use App\Entity\Car;
use App\Repository\ReviewsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class ReviewsController extends AbstractController
{
    #[Route('/api/cars/{id}/latest-reviews', methods: ['GET'])]
    public function getLatestReviews(Car $car, ReviewsRepository $reviewsRepository, SerializerInterface $serializer): Response
    {
        $reviews = $reviewsRepository->findLatestReviewsForCar($car);
        $jsonContent = $serializer->serialize($reviews, 'json');
        return new Response($jsonContent, Response::HTTP_OK, ['Content-Type' => 'application/json']);
    }
}
```

This documentation provides a clear and concise overview of the project and its components, ensuring maintainability and ease of use for future developers and users.