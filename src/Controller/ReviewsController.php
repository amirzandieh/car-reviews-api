<?php

// src/Controller/LatestCarReviewsController.php
namespace App\Controller;

use App\Entity\Car;
use App\Repository\ReviewsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class ReviewsController extends AbstractController
{
    #[Route('/api/cars/{id}/latest-reviews', methods: ['GET'])]
    public function getLatestReviews(Car $car, ReviewsRepository $reviewsRepository, SerializerInterface $serializer): Response
    {
        $reviews = $reviewsRepository->findLatestReviewsForCar($car);

        // Serialize the result using the Serializer component
        $jsonContent = $serializer->serialize($reviews, 'json');

        // Return a new Response with the serialized content
        return new Response($jsonContent, Response::HTTP_OK, ['Content-Type' => 'application/json']);
    }
}
