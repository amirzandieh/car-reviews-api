<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Metadata\ApiResource;

#[ORM\Entity]
#[ApiResource]
class Reviews
{
    #[ORM\Id, ORM\GeneratedValue, ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'integer')]
    #[Assert\NotBlank]
    #[Assert\Range(min: 1, max: 10)]
    private $starRating;

    #[ORM\Column(type: 'text')]
    #[Assert\NotBlank]
    private $reviewText;

    #[ORM\ManyToOne(targetEntity: Car::class)]
    #[ORM\JoinColumn(nullable: false)]
    private $car;

    // Getters and setters...

    /**
     * @return mixed
     */
    public function getStarRating()
    {
        return $this->starRating;
    }

    /**
     * @param mixed $starRating
     */
    public function setStarRating($starRating): Reviews
    {
        $this->starRating = $starRating;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReviewText()
    {
        return $this->reviewText;
    }

    /**
     * @param mixed $reviewText
     */
    public function setReviewText($reviewText): Reviews
    {
        $this->reviewText = $reviewText;
        return $this;
    }
}
