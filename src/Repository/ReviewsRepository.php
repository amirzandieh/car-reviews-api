<?php

namespace App\Repository;

use App\Entity\Car;
use App\Entity\Reviews;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Tools\Pagination\Paginator;

class ReviewsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Reviews::class);
    }

    public function findLatestReviewsForCar(Car $car)
    {
        $query = $this->createQueryBuilder('r')
            ->andWhere('r.car = :car')
            ->andWhere('r.starRating > :rating')
            ->setParameter('car', $car)
            ->setParameter('rating', 6)
            ->orderBy('r.id', 'DESC')
            ->setMaxResults(5)
            ->getQuery();

        return new Paginator($query, $fetchJoinCollection = true);
    }
}
