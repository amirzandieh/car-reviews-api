<?php

namespace App\Tests\Controller;

use App\Entity\Car;
use App\Repository\CarRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ReviewsControllerTest extends WebTestCase
{
    public function testGetLatestReviews(): void
    {
        $client = static::createClient();
        $container = static::getContainer();

        // Assuming you have a method in CarRepository to find a car by some criteria
        $carRepository = $container->get(CarRepository::class);
        $car = $carRepository->findOneBy(['brand' => 'Toyota']);

        $client->request('GET', '/api/cars/' . $car->getId() . '/latest-reviews');

        $this->assertResponseIsSuccessful();
    }
}
