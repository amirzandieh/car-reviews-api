<?php

namespace App\Tests\Entity;

use App\Entity\Car;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CarTest extends KernelTestCase
{
    public function testCreateCar(): void
    {
        self::bootKernel();
        $car = (new Car())
            ->setBrand('Toyota')
            ->setModel('Corolla')
            ->setColor('Blue');

        $errors = self::getContainer()->get('validator')->validate($car);

        $this->assertCount(0, $errors);
    }
}
