<?php

namespace App\Tests\Entity;

use App\Entity\Reviews;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ReviewsTest extends KernelTestCase
{
    public function testCreateReview(): void
    {
        self::bootKernel();
        $review = (new Reviews())
            ->setStarRating(8)
            ->setReviewText('Great car!');

        $errors = self::getContainer()->get('validator')->validate($review);

        $this->assertCount(0, $errors);
    }
}
